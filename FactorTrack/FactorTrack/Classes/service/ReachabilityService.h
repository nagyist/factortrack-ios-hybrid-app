//
//  ReachabilityService.h
//  FactorTrack
//
//  Created by Oscar Lakra on 2/27/14.
//  Copyright (c) 2014 Bayer HealthCare. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Reachability;

@interface ReachabilityService : NSObject

@property (strong, nonatomic) Reachability *reachability;

#pragma mark -
#pragma mark Shared Manager
+ (ReachabilityService *)sharedService;

#pragma mark -
#pragma mark Class Methods
+ (BOOL)isReachable;
+ (BOOL)isUnreachable;
+ (BOOL)isReachableViaWWAN;
+ (BOOL)isReachableViaWiFi;

@end
