/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  NotificationService.h
 *  FactorTrack
 *
 *  Created by Oscar Lakra
 *  Copyright (c) 2014 Bayer HealthCare. All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#import <Foundation/Foundation.h>

@interface NotificationService : NSObject

+ (NotificationService *)notificationService;

    // - (void)scheduleLocalNotifications;
    // - (void)scheduleMoreLocalNotifications;

- (void)scheduleNotificationForDate:(NSDate*)date;

- (void)deleteNotificationForDate:(NSDate*)date;

- (void)deleteAllNotifications;

@end
