/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  NotificationService.m
 *  FactorTrack
 *
 *  Created by Oscar Lakra
 *  Copyright (c) 2014 Bayer HealthCare. All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#import "NotificationService.h"

#define MAX_LOCAL_NOTIFICATIONS_PER_APP (64)

#define SECONDS_IN_DAYS(days) (60 * 60 * 24 * (days))

#define LOCAL_NOTIFICATION_STRING (@"Today at %@\nIt is time.")

static NotificationService *instance = nil;

@implementation NotificationService

+ (NotificationService *)notificationService
{
    @synchronized(self) {
        if(!instance) {
            instance = [[NotificationService alloc] init];
        }
    }
    
    return instance;
}

- (void)scheduleNotificationForDate:(NSDate*)date
{
    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc] init];
    [tempFormatter setDateFormat:@"h:mm a"];
    
    NSString *notificationMessage = [NSString stringWithFormat:@"Today at %@\nIt is time.", [tempFormatter stringFromDate:date]];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = date;
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.alertBody = notificationMessage;
    //localNotification.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)deleteNotificationForDate:(NSDate*)date
{
    NSArray *notifications = [UIApplication sharedApplication].scheduledLocalNotifications;
    for(UILocalNotification *notification in notifications) {
        if([date compare:notification.fireDate] == NSOrderedSame)
           [[UIApplication sharedApplication] cancelLocalNotification:notification];
    }
}

- (void)deleteAllNotifications
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

/*
- (void) scheduleLocalNotificationForDate:(NSDate*)date {
    Settings *settings = [SettingsManager sharedSettingsManager].settings;
    
    if (!settings.isAlertEnabled)
        return;
    
    NSDateFormatter *tempFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [tempFormatter setDateFormat:@"h:mm a"];
    NSString *notificationMessage = [NSString stringWithFormat:LOCAL_NOTIFICATION_STRING, [tempFormatter stringFromDate:date]];
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = date;
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.alertBody = notificationMessage;
    localNotification.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    [localNotification release];
    
}

- (void) scheduleLocalNotificationsInBackground {
    @synchronized(self) {
        NSAutoreleasePool *autoreleasePool = [[NSAutoreleasePool alloc] init];
        Settings *settings = [SettingsManager sharedSettingsManager].settings;
        
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        
        if (!settings.isAlertEnabled)
            return;
        
        if (settings.isOnDemand) {
            NSManagedObjectContext *moc = ((FactorTrackAppDelegate*)[UIApplication sharedApplication].delegate).managedObjectContext;
            
            NSArray *futureUserScheduledInfusions = [[ScheduleManager sharedScheduleManager] retrieveFutureUserScheduledInfusions:moc];
            
            for (Infusion *infusion in futureUserScheduledInfusions) {
                [self scheduleLocalNotificationForDate:infusion.date];
            }
            
            return;
        }
        
        NSDate *date = [NSDate date];
        NSDate *time = [settings alertTimeToDate];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:time];
        
        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        NSInteger second = [components second];
        
        NSDateFormatter *tempFormatter = [[[NSDateFormatter alloc] init] autorelease];
        [tempFormatter setDateFormat:@"h:mm a"];
        NSString *notificationMessage = [NSString stringWithFormat:LOCAL_NOTIFICATION_STRING, [tempFormatter stringFromDate:time]];
        
        [tempFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        if (settings.prophylaxisType == ProphylaxisTypeCustom) {
            NSArray *arr = settings.customDays;
            
            NSDateComponents *components;
            UILocalNotification *localNotification;
            
            for (int i = 1; i <= 7; i++) {
                components = [calendar components:NSWeekdayCalendarUnit fromDate:date];
                
                if ([arr indexOfObject:[NSNumber numberWithInteger:[components weekday]]] != NSNotFound) {
                    components = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
                    NSInteger day = [components day];
                    NSInteger month = [components month];
                    NSInteger year = [components year];
                    NSString* dateTimeRep = [NSString stringWithFormat:@"%04d-%02d-%02d %02d:%02d:%02d", year, month, day, hour, minute, second];
                    
                    localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = [tempFormatter dateFromString:dateTimeRep];
                    localNotification.timeZone = [NSTimeZone localTimeZone];
                    localNotification.alertBody = notificationMessage;
                    localNotification.repeatInterval = NSWeekCalendarUnit;
                    localNotification.applicationIconBadgeNumber = 1;
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                    [localNotification release];
                }
                
                date = [date dateByAddingTimeInterval:SECONDS_IN_DAYS(1)];
            }
        }
        else {
            UILocalNotification *localNotification;
            for (int i = 1; i <= MAX_LOCAL_NOTIFICATIONS_PER_APP; i++) {
                components = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
                NSInteger day = [components day];
                NSInteger month = [components month];
                NSInteger year = [components year];
                NSString* dateTimeRep = [NSString stringWithFormat:@"%04d-%02d-%02d %02d:%02d:%02d", year, month, day, hour, minute, second];
                
                localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [tempFormatter dateFromString:dateTimeRep];
                localNotification.alertBody = notificationMessage;
                localNotification.timeZone = [NSTimeZone localTimeZone];
                localNotification.applicationIconBadgeNumber = 1;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                [localNotification release];
                
                date = [date dateByAddingTimeInterval:SECONDS_IN_DAYS(2)];
            }
        }
        [autoreleasePool release];
    }
}

- (void) scheduleLocalNotifications {
    [self performSelectorInBackground:@selector(scheduleLocalNotificationsInBackground) withObject:nil];
}

- (UILocalNotification*) getLatestLocalNotification:(NSArray*)dateArr {
    NSInteger cnt = [dateArr count];
    
    if (cnt == 0)
        return nil;
    
    UILocalNotification *latestDate = [dateArr objectAtIndex:cnt - 1];
    UILocalNotification *date;
    for (int i = cnt - 2; i >= 0; i--) {
        date = [dateArr objectAtIndex:i];
        if ([((UILocalNotification*)latestDate).fireDate compare:((UILocalNotification*)date).fireDate] == NSOrderedAscending) {
            latestDate = date;
        }
    }
    return latestDate;
}

- (void) scheduleMoreLocalNotifications {
    // Keep 64 local notifications scheduled when Prophylaxis.
    SettingsManager* settingsManager = [SettingsManager sharedSettingsManager];
    if (settingsManager.settings.prophylaxisType == ProphylaxisTypeEveryOtherDay && !(settingsManager.settings.isOnDemand || !settingsManager.settings.isAlertEnabled)) {
        NSArray *localNotifications = [UIApplication sharedApplication].scheduledLocalNotifications;
        UILocalNotification *latestNotification = [self getLatestLocalNotification:localNotifications];
        
        if (!latestNotification) {
            [self scheduleLocalNotifications];
        }
        else {
            if (settingsManager.settings.prophylaxisType == ProphylaxisTypeEveryOtherDay) {
                UILocalNotification *newNotification = latestNotification;
                for (int i = 1; i <= MAX_LOCAL_NOTIFICATIONS_PER_APP - [localNotifications count]; i++) {
                    newNotification = [newNotification copy];
                    newNotification.fireDate = [newNotification.fireDate dateByAddingTimeInterval:SECONDS_IN_DAYS(2)];
                    [[UIApplication sharedApplication] scheduleLocalNotification:newNotification];
                    [newNotification release];
                }
            }
        }
    }
}
*/
@end

