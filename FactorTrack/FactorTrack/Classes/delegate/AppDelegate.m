/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  AppDelegate.m
 *  FactorTrack
 *
 *  Created by Oscar Lakra
 *  Copyright (c) 2014 Bayer HealthCare. All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#import "AppDelegate.h"
#import "ReachabilityService.h"

#define kLocalNotificationAlertTag (3)

@interface AppDelegate ()
{
    Reachability *_reachability;
}

- (void)monitorAvailibility:(NSString *)url;

@end

@implementation AppDelegate

@synthesize window = _window;
@synthesize appUrl = _appUrl;

/*
- (void)monitorAvailibility:(NSString *)url
{
    if(url)
        {
        _reachability = [Reachability reachabilityWithHostname:url];
        _reachability.reachableBlock = ^(Reachability*reach) {
            NSLog(@"%@ is AVAILABLE!", url);
        };
        _reachability.unreachableBlock = ^(Reachability*reach) {
            NSLog(@"%@ CANNOT be reached!", url);
        };
        
        [_reachability startNotifier];
    }
}
*/

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if(!url) return NO;
    
    NSString *URLString = [url absoluteString];
    NSLog(@"Application loaded with url %@", URLString);
    
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AppConfig" ofType:@"plist"];
    NSDictionary *rootDictionary = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSString *_key = @"webapp.url";
    
    if(_appUrl)
        _appUrl = nil;
    
    for(id key in rootDictionary) {
        if([key isEqualToString:_key])
            _appUrl = [rootDictionary objectForKey:key];
    }
    
    [ReachabilityService sharedService];
    
    //[self monitorAvailibility:_appUrl];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"FactorTrack" message:notification.alertBody delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"View", nil];
    alertView.tag = kLocalNotificationAlertTag;
    [alertView show];
}

@end
