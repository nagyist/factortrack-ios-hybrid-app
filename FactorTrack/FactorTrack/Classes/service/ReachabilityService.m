//
//  ReachabilityService.m
//  FactorTrack
//
//  Created by Oscar Lakra on 2/27/14.
//  Copyright (c) 2014 Bayer HealthCare. All rights reserved.
//

#import "ReachabilityService.h"
#import <Reachability/Reachability.h>

@implementation ReachabilityService

#pragma mark - Default Manager
+ (ReachabilityService *)sharedService {
    static ReachabilityService *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

#pragma mark -
#pragma mark Memory Management
- (void)dealloc {
        // Stop Notifier
    if (_reachability) {
        [_reachability stopNotifier];
    }
}

#pragma mark -
#pragma mark Class Methods
+ (BOOL)isReachable {
    return [[[ReachabilityService sharedService] reachability] isReachable];
}

+ (BOOL)isUnreachable {
    return ![[[ReachabilityService sharedService] reachability] isReachable];
}

+ (BOOL)isReachableViaWWAN {
    return [[[ReachabilityService sharedService] reachability] isReachableViaWWAN];
}

+ (BOOL)isReachableViaWiFi {
    return [[[ReachabilityService sharedService] reachability] isReachableViaWiFi];
}

#pragma mark -
#pragma mark Private Initialization
- (id)init {
    self = [super init];
    
    if (self) {
            // Initialize Reachability
        self.reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
        
            // Start Monitoring
        [self.reachability startNotifier];
    }
    
    return self;
}

@end
