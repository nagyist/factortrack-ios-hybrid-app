/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  ViewController.m
 *  FactorTrack
 *
 *  Created by Oscar Lakra
 *  Copyright (c) 2014 Bayer HealthCare. All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#import "AppDelegate.h"
#import "ViewController.h"
#import "NotificationService.h"

#import <Reachability/Reachability.h>

@interface ViewController ()
{
    UIWebView *_webView;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect frame = self.view.frame;
    if([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending) {
        frame.origin.y += 20.0f;
        frame.size.height -= 20.0f;
    } else {
        frame.origin.y -= 20.0f;
        frame.size.height += 20.0f;
    }
    
    UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default.png"]];
    bgView.frame = frame;
    [self.view addSubview:bgView];
    
    if(!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:frame];
        _webView.backgroundColor = [UIColor clearColor];
        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _webView.delegate = self;
    }
    [self.view addSubview: _webView];
    
    AppDelegate *__app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:__app.appUrl]];
    [_webView loadRequest:request];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        NSLog(@"Reachable");
    } else {
        NSLog(@"Un Reachable");
        UIAlertView *_alert = [[UIAlertView alloc] initWithTitle:@"No Connectivity" message:@"Cannot reach the FactorTrack service. Please check if you have access to the internet." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [_alert show];
    }
}

#pragma mark - UIWebViewDelegate methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *requestURLString = [[request URL] absoluteString];
    
    NSLog(@"url.scheme: %@", request.URL.scheme);
    
    if ([requestURLString hasPrefix:@"ft-call:"]) {
        
        NSArray *components = [requestURLString componentsSeparatedByString:@":"];
        NSString *commandName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSData *argsData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *args = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:argsData options:kNilOptions error:&error];
        
        if([commandName isEqualToString:@"UPDATE-ALERT-TIME"]) {
            [self processUpdateAlertTime:args];
        }
        
        NSLog(@"Command: %@ - %@", commandName, [args description]);
        
        return NO;
        
    } else {
        
        return YES;
    }
}

#pragma mark - Private methods

- (void)processUpdateAlertTime:(NSDictionary *)settings
{
    NSNumber *alertNotificationEnabled = (NSNumber *)[settings objectForKey:@"alertEmailEnabled"];
    BOOL notificationEnabled = ([alertNotificationEnabled intValue] == 0)? NO : YES;
    
    NSString *alertTimeZone = (NSString *)[settings objectForKey:@"timeZone"];
    
    NSString *alertEmailTime = (NSString *)[settings objectForKey:@"alertEmailTime"];
    
    NotificationService *service = [NotificationService notificationService];
    
    if(notificationEnabled) {
        [self buildAlertTime:[self parseAlertTime:alertEmailTime]];
        /*
         
        NSArray *alertTime = [self alertTimeComponents:alertEmailTime];
        NSLog(@"update all alert times from source:%@ time:%@", [self parseAlertTime:alertEmailTime], alertTime);
        
        NSLog(@"today: %@", [NSDate date]);
        
        NSCalendar* myCalendar = [NSCalendar currentCalendar];
        [myCalendar setTimeZone:[NSTimeZone localTimeZone]];
        
        NSDateComponents* components = [myCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
        [components setHour: (NSInteger)[alertTime objectAtIndex:0]];
        [components setMinute: (NSInteger)[alertTime objectAtIndex:1]];
        [components setSecond: (NSInteger)[alertTime objectAtIndex:2]];
        NSDate *today = [myCalendar dateFromComponents:components];
        NSLog(@"Setting alert for : %@", today);
        
        //[service scheduleNotificationForDate:today];
        */
        
    } else {
        
        [service deleteAllNotifications];

    }
}

- (NSString *)parseAlertTime:(NSString *)setting
{
    NSRange start = [setting rangeOfString:@"1970-01-01T"];
    NSRange end = [setting rangeOfString:@".000Z"];
    
    NSRange range = { start.length + start.location, end.location - (start.length + start.location)};
    
    NSString *alertTime = [setting substringWithRange:range];
    return alertTime;
}

- (NSArray *)alertTimeComponents:(NSString *)setting
{
    NSString *time = [self parseAlertTime:setting];
    
    return [time componentsSeparatedByString:@":"];
}

- (void)buildAlertTime:(NSString *)setting
{
    NSDateFormatter *in = [[NSDateFormatter alloc] init];
    [in setDateFormat:@"yyyy-MM-dd"];
    
    NSDateFormatter *out = [[NSDateFormatter alloc] init];
    [out setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    #if !TARGET_IPHONE_SIMULATOR
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [in setLocale:locale];
        [out setLocale:locale];
    #endif
    
    NSString *time = [NSString stringWithFormat:@"%@ %@", [in stringFromDate:[NSDate date]], setting];
    NSDate *alertTime = [out dateFromString:time];
    
    NSTimeZone *currentTimeZone = [NSTimeZone timeZoneWithName:@"America/New_York"];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:alertTime];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:alertTime];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;

    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:alertTime];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm"];
    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
    [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatters setDoesRelativeDateFormatting:YES];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *dateStr = [dateFormatters stringFromDate: destinationDate];
    NSLog(@"DateString : %@", dateStr);
    
    NotificationService *service = [NotificationService notificationService];
    [service scheduleNotificationForDate:alertTime];
}

/*
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = request.URL;
    
    if(![url.scheme isEqualToString:@"http"] && ![url.scheme isEqualToString:@"https"]) {
        if([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
            return NO;
        }
        
        NSLog(@"url being loaded: [%@]", url);
        
        // UPDATE-ALERT-TIME
        
        if([[url absoluteString] hasPrefix:@"ft-call:"]) {
            NSLog(@"url received: %@", [url absoluteString]);
            
            NSArray *components = [[url absoluteString] componentsSeparatedByString:@":"];
            NSString *commandName = (NSString*)[components objectAtIndex:1];
            NSString *argsAsString = [(NSString*)[components objectAtIndex:2]
                                      stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSError *error = nil;
            NSData *argsData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *args = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:argsData options:kNilOptions error:&error];
            
            NSLog(@"Command: %@ - %@", commandName, [args description]);
            
            if ([[commandName uppercaseString] isEqualToString:@"UPDATE-ALERT-TIME"]) {
                NSArray *scheduled = [[UIApplication sharedApplication] scheduledLocalNotifications];
                NSMutableArray *newSchedule = [NSMutableArray array];
                if(scheduled) {
                    BOOL alertEnabled = (BOOL)[args objectForKey:@"alertEmailEnabled"];
                    NSString *alertTime = [args objectForKey:@"alertEmailTime"];
                    NSRange tRange = [alertTime rangeOfString:@"T"];
                    NSRange dotRange = [alertTime rangeOfString:@"."];
                    
                    NSRange time;
                    time.length = dotRange.location - (tRange.location + 1);
                    time.location = tRange.location + 1;
                    
                    NSArray *newAlertTime = [[alertTime substringWithRange:time] componentsSeparatedByString:@":"];
                    
                    if(alertEnabled) {
                        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                        NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday) fromDate:[NSDate date]];
                        NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                        [dateComps setHour:[((NSString *)[newAlertTime objectAtIndex:0]) intValue]];
                        [dateComps setMinute:[((NSString *)[newAlertTime objectAtIndex:1]) intValue]];
                        NSDate *notifyDate = [calendar dateFromComponents:dateComps];
                        
                        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
                        if (localNotif) {
                            localNotif.fireDate = [notifyDate dateByAddingTimeInterval:-(5 * 60)];
                            localNotif.timeZone = [NSTimeZone defaultTimeZone];
                            localNotif.alertBody = [NSString stringWithFormat:NSLocalizedString(@"%@ in %i minutes.", nil),
                                                    @"Infusion", 5];
                            localNotif.alertAction = NSLocalizedString(@"View Details", nil);
                            
                            localNotif.soundName = UILocalNotificationDefaultSoundName;
                            
                            NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"ftMonAlert" forKey:@"ftMonAlertKey"];
                            localNotif.userInfo = infoDict;
                            
                            [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
                        }
                    } else {
                        [[UIApplication sharedApplication] cancelAllLocalNotifications];
                    }
                }
            }
            
            return NO;
        }
        
        return YES;
        }
    
    return YES;
}
*/

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webView started loading content");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webView finished loading content");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if(error.code == NSURLErrorCancelled) return;
    
    if(error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"]) return;
    
    // Normal error handling ...
}

@end

